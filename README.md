## README

workspace for Bodo	Winter's[^1] *Linear	models	and	linear	mixed	effects	models	in	R:	Tutorials*



- http://www.bodowinter.com/tutorial/bw_LME_tutorial1.pdf

- http://www.bodowinter.com/tutorial/bw_LME_tutorial2.pdf


Please	cite	as:
Winter,	B.	(2013).	Linear	models	and	linear	mixed	effects	models	in	R	with	linguistic	applications.	arXiv:1308.5499.	[http://arxiv.org/pdf/1308.5499.pdf]




[^1]:  Bodo is at University	of	California,	Merced,	Cognitive	and	Information	Sciences ; For	 updates	 and	 other	 tutorials,	 check	 his	 [webpage](http://www.bodowinter.com).	 

